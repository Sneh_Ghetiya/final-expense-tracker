const db = require("./src/models/index");
const express = require("express");
const { ApolloServer, gql } = require("apollo-server");
const typeDefs = require("./src/controllers/schemas/schema");
const resolvers = require("./src/controllers/resolvers/resolvers");
require("dotenv").config();

const PORT = 4000;
const ENDPOINT = "/graphql";

const server = new ApolloServer({ cors: true, typeDefs, resolvers });

server.listen().then(() => {
	console.log(`Server 🚀 on http://localhost:${PORT}${ENDPOINT}`);
});

db.sequelize.sync().then(() => {
	console.log("Database connected");
});
