const dbConfig = require("../config/db.config");
const Sequelize = require("sequelize");

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
	host: dbConfig.HOST,
	dialect: dbConfig.dialect,
	operatorsAliases: 0,

	pool: {
		max: dbConfig.pool.max,
		min: dbConfig.pool.min,
		acquire: dbConfig.pool.acquire,
		idle: dbConfig.pool.idle,
	},
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = require("./user.model")(sequelize, Sequelize);
db.transactions = require("./transaction.model")(sequelize, Sequelize);
db.categories = require("./category.model")(sequelize, Sequelize);

db.users.hasMany(db.transactions, { as: "transactions" });
db.transactions.belongsTo(db.users, {
	foreignKey: "userId",
	as: "user",
});

db.categories.hasMany(db.transactions, { as: "transactions" });
db.transactions.belongsTo(db.categories, {
	foreignKey: "categoryId",
	as: "category",
});

module.exports = db;
