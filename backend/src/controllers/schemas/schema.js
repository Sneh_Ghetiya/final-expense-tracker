const { gql } = require("apollo-server");

const typeDefs = gql`
	type User {
		id: ID!
		name: String!
		limit: Int!
	}
	type Transaction {
		id: ID!
		description: String!
		amount: Int!
		file_path: String!
		userId: Int!
		categoryId: Int!
	}
	type Category {
		id: ID!
		name: String!
	}
	type Query {
		getUser(id: ID!): User
		getTransactions(userId: ID!): [Transaction]
		getCategories: [Category]
	}
	type Mutation {
		createUser(name: String!, limit: Int!): User
		createTransaction(
			description: String!
			amount: Int!
			file_path: String!
			userId: Int!
			categoryId: Int!
		): Transaction
		createCategory(name: String!): Category
	}
`;

module.exports = typeDefs;
