const db = require("../../models/index");
const User = db.users;
const Transaction = db.transactions;
const Category = db.categories;

const resolvers = {
	Query: {
		getUser(_, { id }) {
			if (id) {
				return User.findOne({ where: { id } });
			}
		},
		getTransactions(_, args) {
			if (args.userId) {
				return Transaction.findAll({
					where: {
						userId: args.userId,
					},
				});
			}
		},
		getCategories(_, args) {
			return Category.findAll();
		},
	},
	Mutation: {
		async createUser(_, { name, limit }) {
			if ((name, limit)) {
				const user = await User.create({ name: name, limit: limit });
				return user;
			} else {
				return null;
			}
		},
		async createTransaction(
			_,
			{ description, amount, file_path, userId, categoryId }
		) {
			const transaction = await Transaction.create({
				description: description,
				amount: amount,
				file_path: file_path,
				userId: userId,
				categoryId: categoryId,
			});

			return transaction;
		},
		async createCategory(_, { name }) {
			if (name) {
				const category = await Category.create({ name: name });
				return category;
			} else {
				return null;
			}
		},
	},
};

module.exports = resolvers;
