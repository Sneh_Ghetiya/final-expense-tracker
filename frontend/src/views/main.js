import React, { useEffect, useState } from "react";
// import * as Icon from "react-bootstrap-icons";
import "./style.css";

const Main = () => {
	const [limit, setLimit] = useState();
	const [category, setCategory] = useState();

	function handleClick(event) {
		setCategory(event.target.value);
		console.log(category);
	}

	function handleSelect(event) {
		console.log("fired");
		console.log(event.target.value);
	}

	// useEffect(() => {
	// 	setLimit(document.getElementById("limit").value);
	// }, []);

	let date = new Date().toLocaleDateString();
	return (
		<form>
			<div className="container">
				<h1 className="main-head">Expense Tracker</h1>
				<div
					className="row mt-6"
					style={{ marginLeft: "5px", marginRight: "5px" }}
				>
					<div
						className="col-sm"
						style={{
							display: "flex",
							textAlign: "left",
							flexDirection: "row",
							justifyContent: "space-between",
							paddingBottom: "35px",
						}}
					>
						<div
							className="left"
							style={{
								float: "left",
								flexDirection: "row",
								alignItems: "center",
								justifyContent: "space-evenly",
								marginLeft: "50px",
							}}
						>
							<h1 className="heading" style={{ color: "#ffffff" }}>
								Categories
							</h1>
						</div>
						<div
							className="col-sm"
							style={{
								display: "flex",
								flexDirection: "row",
								alignItems: "center",
								justifyContent: "space-evenly",
								border: "none",
								borderRadius: "50px",
								backgroundColor: "rgba(255, 255, 255, 0.3)",
							}}
						>
							<div
								className="alert alert-success"
								style={{
									flexDirection: "row",
									padding: "20px 0px 20px 0px",
									marginLeft: "25px",
								}}
							>
								<button
									style={{
										padding: "20px 30px 20px 30px",
										borderRadius: "12px",
										marginRight: "25px",
										border: "none",
										boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
										backgroundColor: "rgba(255, 255, 255, 0.75)",
									}}
									value={"clothes"}
									onClick={(event) => {
										handleClick(event);
									}}
								>
									Clothes
								</button>
							</div>
							<div
								className="alert alert-success"
								style={{
									flexDirection: "row",
									padding: "20px 0px 20px 0px",
									marginLeft: "25px",
								}}
							>
								<button
									style={{
										padding: "20px 30px 20px 30px",
										borderRadius: "12px",
										marginRight: "25px",
										border: "none",
										boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
										backgroundColor: "rgba(255, 255, 255, 0.75)",
									}}
									value={"food"}
									onClick={(event) => {
										handleClick(event);
									}}
								>
									Food
								</button>
							</div>
							<div
								className="alert alert-success"
								style={{
									flexDirection: "row",
									padding: "20px 0px 20px 0px",
									marginLeft: "25px",
								}}
							>
								<button
									style={{
										padding: "20px 30px 20px 30px",
										borderRadius: "12px",
										marginRight: "25px",
										border: "none",
										boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
										backgroundColor: "rgba(255, 255, 255, 0.75)",
									}}
									value={"petrol"}
									onClick={(event) => {
										handleClick(event);
									}}
								>
									Petrol
								</button>
							</div>
							<div
								className="alert alert-success"
								style={{
									flexDirection: "row",
									padding: "20px 0px 20px 0px",
									marginLeft: "25px",
								}}
							>
								<button
									style={{
										padding: "20px 30px 20px 30px",
										borderRadius: "12px",
										marginRight: "25px",
										border: "none",
										boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
										backgroundColor: "rgba(255, 255, 255, 0.75)",
									}}
									value={"grocery"}
									onClick={(event) => {
										handleClick(event);
									}}
								>
									Grocery
								</button>
							</div>
							<div
								className="alert alert-success"
								style={{
									flexDirection: "row",
									padding: "20px 0px 20px 0px",
									marginLeft: "25px",
								}}
							>
								<button
									style={{
										padding: "20px 30px 20px 30px",
										borderRadius: "12px",
										marginRight: "25px",
										border: "none",
										boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
										backgroundColor: "rgba(255, 255, 255, 0.75)",
									}}
									value={"salary"}
									onClick={(event) => {
										handleClick(event);
									}}
								>
									Salary
								</button>
							</div>
							<div
								className="alert alert-success"
								style={{
									flexDirection: "row",
									padding: "20px 0px 20px 0px",
									marginLeft: "25px",
								}}
							>
								<button
									style={{
										padding: "20px 30px 20px 30px",
										borderRadius: "12px",
										marginRight: "25px",
										border: "none",
										boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
										backgroundColor: "rgba(255, 255, 255, 0.75)",
									}}
									value={"loan"}
									onClick={(event) => {
										handleClick(event);
									}}
								>
									Loan
								</button>
							</div>
						</div>
					</div>
					<div
						style={{
							border: "none",
							backgroundColor: "rgba(255, 255, 255, 0.3)",
							borderRadius: "12px",
							paddingBottom: "10px",
						}}
					>
						<div
							className="div-2"
							style={{
								display: "flex",
								justifyContent: "space-between",
								flexWrap: "wrap",
								alignContent: "space-between",
								margin: "0px 10px 0px 29px",
							}}
						>
							<div
								className="left"
								style={{ flexDirection: "row", alignItems: "left" }}
							>
								<h1 style={{ color: "#000000" }}>Description</h1>
							</div>
							<div
								className="center-1"
								style={{ flexDirection: "row", alignItems: "center" }}
							>
								<div className="form-group">
									<select
										class="custom-select"
										style={{
											marginTop: "25px",
											padding: "10px 20px 10px 20px",
										}}
									>
										<option selected>Select your limit</option>
										<option
											value="1000"
											onClick={(event) => {
												handleSelect(event);
											}}
										>
											1000
										</option>
										<option value="2000">2000</option>
										<option value="3000">3000</option>
										<option value="4000">4000</option>
										<option value="5000">5000</option>
										<option value="6000">6000</option>
										<option value="7000">7000</option>
										<option value="8000">8000</option>
										<option value="9000">9000</option>
									</select>
								</div>
							</div>
							<div
								className="center-2"
								style={{ flexDirection: "row", alignItems: "center" }}
							>
								<div className="form-group">
									<input
										type="file"
										className="form-control-file"
										id="exampleFormControlFile1"
										style={{
											marginTop: "25px",
											padding: "10px 20px 10px 20px",
										}}
									/>
								</div>
							</div>
							<div
								className="right"
								style={{ flexDirection: "row", alignItems: "center" }}
							>
								<button
									style={{
										position: "relative",
										justifyContent: "center",
										alignItems: "center",
										display: "block",
										marginTop: "25px",
										padding: "5px 30px 5px 30px",
										backgroundColor: "rgba(255, 255, 255, 0.75)",
										border: "none",
										borderRadius: "15px",
										fontSize: "25px",
										boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
									}}
								>
									+
								</button>
							</div>
						</div>
						<div
							style={{
								backgroundColor: "rgba(255, 255, 255, 0.7)",
								margin: "0px 20px 20px 20px",
								border: "none",
								borderRadius: "15px",
								marginTop: "10px",
								boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
							}}
						>
							<div
								className="div-2"
								style={{
									display: "flex",
									justifyContent: "space-between",
									alignItems: "center",
								}}
							>
								<div
									className="left"
									style={{
										flexDirection: "row",
										alignItems: "center",
										margin: "0px 30px 0px 30px",
									}}
								>
									<h1 style={{ color: "#000000" }}>Lassi</h1>
									<h3 style={{ color: "#000000" }}>Rs. 35</h3>
								</div>
								<div
									className="center"
									style={{
										flexDirection: "row",
										alignItems: "center",
										margin: "0px 30px 0px 30px",
									}}
								>
									<h1 style={{ color: "#000000" }}>{date}</h1>
								</div>
								<div
									className="right"
									style={{
										flexDirection: "row",
										alignItems: "center",
										margin: "0px 30px 0px 30px",
									}}
								>
									{/* <Icon.Download
										style={{ height: "25px", width: "25px", color: "#000000" }}
									/> */}
								</div>
							</div>
							<div
								className="div-2"
								style={{
									display: "flex",
									justifyContent: "space-between",
									alignItems: "center",
								}}
							>
								<div
									className="left"
									style={{
										flexDirection: "row",
										alignItems: "center",
										margin: "0px 30px 0px 30px",
									}}
								>
									<h1 style={{ color: "#000000" }}>Lassi</h1>
									<h3 style={{ color: "#000000" }}>Rs. 35</h3>
								</div>
								<div
									className="center"
									style={{
										flexDirection: "row",
										alignItems: "center",
										margin: "0px 30px 0px 30px",
									}}
								>
									<h1 style={{ color: "#000000" }}>{date}</h1>
								</div>
								<div
									className="right"
									style={{
										flexDirection: "row",
										alignItems: "center",
										margin: "0px 30px 0px 30px",
									}}
								>
									{/* <Icon.Download
										style={{ height: "25px", width: "25px", color: "#000000" }}
									/> */}
								</div>
							</div>
							<div
								className="div-2"
								style={{
									display: "flex",
									justifyContent: "space-between",
									alignItems: "center",
								}}
							>
								<div
									className="left"
									style={{
										flexDirection: "row",
										alignItems: "center",
										margin: "0px 30px 0px 30px",
									}}
								>
									<h1 style={{ color: "#000000" }}>Lassi</h1>
									<h3 style={{ color: "#000000" }}>Rs. 35</h3>
								</div>
								<div
									className="center"
									style={{
										flexDirection: "row",
										alignItems: "center",
										margin: "0px 30px 0px 30px",
									}}
								>
									<h1 style={{ color: "#000000" }}>{date}</h1>
								</div>
								<div
									className="right"
									style={{
										flexDirection: "row",
										alignItems: "center",
										margin: "0px 30px 0px 30px",
									}}
								>
									{/* <Icon.Download
										style={{ height: "25px", width: "25px", color: "#000000" }}
									/> */}
								</div>
							</div>
							<div
								className="div-2"
								style={{
									display: "flex",
									justifyContent: "space-between",
									alignItems: "center",
								}}
							>
								<div
									className="left"
									style={{
										flexDirection: "row",
										alignItems: "center",
										margin: "0px 30px 0px 30px",
									}}
								>
									<h1 style={{ color: "#000000" }}>Lassi</h1>
									<h3 style={{ color: "#000000" }}>Rs. 35</h3>
								</div>
								<div
									className="center"
									style={{
										flexDirection: "row",
										alignItems: "center",
										margin: "0px 30px 0px 30px",
									}}
								>
									<h1 style={{ color: "#000000" }}>{date}</h1>
								</div>
								<div
									className="right"
									style={{
										flexDirection: "row",
										alignItems: "center",
										margin: "0px 30px 0px 30px",
									}}
								>
									{/* <Icon.Download
										style={{ height: "25px", width: "25px", color: "#000000" }}
									/> */}
								</div>
							</div>
							<div
								className="div-2"
								style={{
									display: "flex",
									justifyContent: "space-between",
									alignItems: "center",
									border: "none	",
								}}
							>
								<div
									className="left"
									style={{
										flexDirection: "row",
										alignItems: "center",
										margin: "0px 30px 0px 30px",
									}}
								>
									<h1>Total : </h1>
								</div>
								<div
									className="center"
									style={{
										flexDirection: "row",
										alignItems: "center",
										margin: "0px 30px 0px 30px",
									}}
								>
									<div
										style={{ justifyContent: "center", alignItems: "center" }}
									>
										<h2
											style={{
												border: "none",
												padding: "20px 20px",
												backgroundColor: "rgba(255, 255, 255, 1)",
												borderRadius: "15px",
												boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
												color: "red",
											}}
										>
											6000 RS.
										</h2>
									</div>
								</div>
								<div
									className="right"
									style={{
										flexDirection: "row",
										alignItems: "center",
										margin: "0px 30px 0px 30px",
									}}
								>
									<h2
										style={{
											border: "none",
											padding: "20px 20px",
											backgroundColor: "rgba(255, 255, 255, 1)",
											borderRadius: "15px",
											boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
											color: "blue",
										}}
									>
										Limit : 5000 RS.
									</h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	);
};

export default Main;
