import Welcome from "./components/welcome/welcome";
import Tracker from "./components/tracker/tracker";
import Main from "./views/main";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

const App = () => {
	return (
		<Router>
			<div className="App">
				<div>
					<Switch>
						<Route exact path="/" component={Welcome} />
						<Route exact path="/tracker" component={Tracker} />
						<Route exact path="/main" component={Main} />
					</Switch>
				</div>
			</div>
		</Router>
	);
};

export default App;
