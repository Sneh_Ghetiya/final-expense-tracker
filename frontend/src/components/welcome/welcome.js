import React from "react";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import InputGroup from "react-bootstrap/InputGroup";
import Form from "react-bootstrap/Form";
import { FormControl } from "react-bootstrap";
import { useMutation, useLazyQuery } from "@apollo/client";
import { useState } from "react";
import { CREATE_USER } from "../graphql/mutations";
import { GET_USER } from "../graphql/queries";
import { useHistory } from "react-router-dom";

const Welcome = () => {
	const [username, setUsername] = useState("");
	const [limit, setLimit] = useState(0);
	const [createUser] = useMutation(CREATE_USER);
	const [getUser] = useLazyQuery(GET_USER);
	const history = useHistory();

	const handleSubmit = (e) => {
		e.preventDefault();
		createUser({
			variables: {
				name: username,
				limit: parseInt(limit),
			},
		}).then((res) => {
			if (res.data.createUser.id) {
				getUser({ variables: { id: res.data.createUser.id } });
				history.push({
					pathname: `/tracker`,
					state: { user: res.data.createUser },
				});
			}
		});
		setUsername("");
		setLimit(0);
	};

	const handleChange = (e) => {
		if (e.target.name === "username") {
			setUsername(e.target.value);
		} else {
			setLimit(e.target.value);
		}
	};
	return (
		<Container
			style={{
				alignItems: "center",
				justifyContent: "center",
				marginTop: "15%",
			}}
		>
			<h2 style={{ textAlign: "center", marginBottom: "25px" }}>
				Expense Tracker
			</h2>
			<Form
				onSubmit={handleSubmit}
				className="mb-3"
				style={{
					width: "50%",
					alignItems: "center",
					justifyContent: "center",
					marginLeft: "25%",
				}}
			>
				<Form.Group className="mb-3" controlId="formBasicEmail">
					<Form.Control
						type="text"
						placeholder="Enter username"
						name="username"
						onChange={handleChange}
						value={username}
					/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="formBasicPassword">
					<InputGroup
						name="limit"
						className="mb-3"
						style={{
							alignItems: "center",
							justifyContent: "center",
						}}
						onChange={handleChange}
						value={limit}
					>
						<InputGroup.Text>&#x20B9;</InputGroup.Text>
						<FormControl placeholder="Enter your expense limit" />
					</InputGroup>
				</Form.Group>
				<Button variant="primary" type="submit">
					Submit
				</Button>
			</Form>
		</Container>
	);
};

export default Welcome;
