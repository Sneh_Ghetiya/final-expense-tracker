import { useLazyQuery, useMutation, useQuery } from "@apollo/client";
import { useEffect, useState } from "react";
import { Col, Container, Dropdown, Form, Row, Table } from "react-bootstrap";
import { GET_CATEGORIES, GET_TRANSACTIONS } from "../graphql/queries";
import { CREATE_TRANSACTION } from "../graphql/mutations";
import "./tracker.css";

const Tracker = (props) => {
	const { user } = props.location.state ? props.location.state : {};
	var total = 0;
	const [getTransactions, { data: data2 }] = useLazyQuery(GET_TRANSACTIONS);
	const [transactions, setTransactions] = useState({});
	const { id } = user;
	const [categories, setCategories] = useState();
	const [amount, setAmount] = useState(0);
	const [description, setDescription] = useState();
	const [categoryName, setCategoryName] = useState();
	const [categoryId, setCategoryId] = useState();
	const [createTransaction] = useMutation(CREATE_TRANSACTION);
	const { data } = useQuery(GET_CATEGORIES, {});
	const { data: data3 } = useQuery(GET_TRANSACTIONS, {});
	const [danger, setDanger] = useState(false);

	useEffect(() => {
		if (data) {
			setCategories(data.getCategories);
		}
		if (id) {
			getTransactions({ variables: { userId: id } });
		}
		if (data3) {
			console.log(data3.getTransactions);
		}
	}, [data, getTransactions, id, data3]);

	useEffect(() => {
		if (data2) {
			setTransactions(data2.getTransactions);
		}
	}, [data2]);

	if (transactions && transactions.length > 0) {
		total = transactions
			.map((transaction) => transaction.amount)
			.reduce((a, b) => a + b, 0);
		// if (parseInt(total) > parseInt(user.limit)) {
		// 	setDanger(!danger);
		// }
	}

	const handleClick = (e) => {};

	const handleChange = (e) => {
		if (e.target.name === "amount") {
			setAmount(e.target.value);
		} else if (e.target.name === "description") {
			setDescription(e.target.value);
		} else if (e.target.name === "category") {
			setCategoryName(e.target.text);
			setCategoryId(e.target.id);
		}
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		createTransaction({
			variables: {
				description: description,
				amount: parseInt(amount),
				file_path: "dummy",
				userId: parseInt(id),
				categoryId: parseInt(categoryId),
			},
		}).then((res) => {
			if (res.data) {
				getTransactions({ variables: { userId: id } });
			}
		});
		setAmount(0);
		setDescription("");
		setCategoryName("");
	};

	return (
		<div className="tracker-main">
			<div className="container-main">
				<div className="container-div">
					<div className="greetings">
						<h1>Welcome</h1>
					</div>
					<div className="name">
						<h2>{user.name}</h2>
					</div>
				</div>
				<div className="container-div">
					<div className="greetings">
						<h1>Income</h1>
					</div>
					<div className="name">
						<h2>&#8377; {user.limit}</h2>
					</div>
				</div>
			</div>
			<div className="button-group">
				{categories && categories.length > 0 ? (
					categories.map((category) => (
						<div key={category.name} className="wrap">
							<button
								className="button"
								onClick={handleClick}
								value={category.name}
							>
								{category.name}
							</button>
						</div>
					))
				) : (
					<div></div>
				)}
			</div>
			<Form onSubmit={handleSubmit}>
				<div className="user-input-group">
					<Container fluid="md">
						<Row className="justify-content-sm-center">
							<Col xs={6} md={3}>
								<textarea
									className="description-box"
									name="description"
									id="description-box"
									placeholder="Type your expense note"
									rows="2"
									onChange={handleChange}
									value={description}
								></textarea>
							</Col>
							<Col xs={6} md={3}>
								<div className="input-file-container2">
									<div className="rupee-sign">&#8377;</div>
									<input
										name="amount"
										type="text"
										className="amount-field"
										onChange={handleChange}
										value={amount}
									/>
								</div>
							</Col>
							<Col xs={6} md={3}>
								<div className="input-file-container">
									<input type="file" />
								</div>
							</Col>
							<Col xs={6} md={3}>
								<div className="input-file-container">
									<Dropdown>
										<Dropdown.Toggle variant="success" id="dropdown-basic">
											{categoryName ? categoryName : "Select Category"}
										</Dropdown.Toggle>

										<Dropdown.Menu>
											{categories && categories.length > 0 ? (
												categories.map((category) => (
													<Dropdown.Item
														name="category"
														className="select-category"
														id={category.id}
														value={category.name}
														key={category.id}
														onClick={handleChange}
													>
														{category.name}
													</Dropdown.Item>
												))
											) : (
												<div></div>
											)}
										</Dropdown.Menu>
									</Dropdown>
								</div>
							</Col>
						</Row>
						<div className="add-button">
							<button className="button2">Add</button>
						</div>
					</Container>
				</div>
			</Form>
			<Container>
				<Table
					striped
					bordered
					hover
					size="lg"
					responsive
					className="justify-content-md-center"
				>
					<thead>
						<tr>
							<th>Description</th>
							<th>Amount</th>
							<th>File</th>
							<th>Date</th>
						</tr>
					</thead>
					<tbody>
						{transactions && transactions.length > 0 ? (
							transactions.map((transaction) => (
								<tr key={transaction.id}>
									<td>{transaction.description}</td>
									<td>{transaction.amount}</td>
									<td>{transaction.file_path}</td>
									<td>{Date(transaction.createdAt).toLocaleString()}</td>
								</tr>
							))
						) : (
							<tr></tr>
						)}
					</tbody>
				</Table>
			</Container>
			<Container className="footer">
				<div className="total-container">
					<div className="total-tag">
						<h1 className="total">Total</h1>
					</div>
					<div className={danger ? "total-money-red" : "total-money-green"}>
						&#8377;<p className="value">{total}</p>
					</div>
				</div>
			</Container>
		</div>
	);
};

export default Tracker;
