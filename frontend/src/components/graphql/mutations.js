import { gql } from "@apollo/client";

export const CREATE_USER = gql`
	mutation CreateUser($name: String!, $limit: Int!) {
		createUser(name: $name, limit: $limit) {
			id
			name
			limit
		}
	}
`;

export const CREATE_CATEGORY = gql`
	mutation CreateCategory($name: String!) {
		createCategory(name: $name) {
			id
			name
		}
	}
`;

export const CREATE_TRANSACTION = gql`
	mutation CreateTransaction(
		$description: String!
		$amount: Int!
		$file_path: String!
		$userId: Int!
		$categoryId: Int!
	) {
		createTransaction(
			description: $description
			amount: $amount
			file_path: $file_path
			userId: $userId
			categoryId: $categoryId
		) {
			id
			description
			amount
			file_path
			userId
			categoryId
		}
	}
`;
