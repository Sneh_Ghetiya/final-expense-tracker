import { gql } from "@apollo/client";

export const GET_USER = gql`
	query GetUser($id: ID!) {
		getUser(id: $id) {
			id
			name
			limit
		}
	}
`;

export const GET_TRANSACTIONS = gql`
	query GetTransactions($userId: ID!) {
		getTransactions(userId: $userId) {
			id
			description
			amount
			file_path
			userId
			categoryId
		}
	}
`;

export const GET_CATEGORIES = gql`
	query GetCategories {
		getCategories {
			id
			name
		}
	}
`;
